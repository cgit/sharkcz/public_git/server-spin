#!/usr/bin/python
# -*- coding: utf-8 -*-


import os
import yum.comps
import kobo.shortcuts


RELEASEVER = 14

GROUPS = [
    "core",
    "base",
    "console-internet",
    "file-server",
    "mysql",
    "network-file-system-client",
    "network-tools",
    "network-server",
    "postgresql",
    "scm",
    "storage",
    "text-editors",
    "web-server",
]

PACKAGES = [
    "kernel",
    "kernel-PAE",
]

WORK_DIR = "/var/tmp/server-spin"
REPO_DIR = os.path.join(WORK_DIR, "repo")
COMPOSE_DIR = os.path.join(WORK_DIR, "compose")
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
COMPS_FILE = os.path.join(SCRIPT_DIR, "Fedora-Server-comps.xml")
KS_FILE = os.path.join(WORK_DIR, "fedora-server.ks")


def read_comps_packages(comps_file, group=None):
    comps_obj = yum.comps.Comps()
    comps_obj.add(comps_file)
    result = set()
    for i in comps_obj.get_groups():
        if group is None or i.groupid == group:
            result.update(i.packages)
    return sorted(result)


def create_ks(groups, packages):
    lines = []
    lines.append("repo --name=fedora --mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=fedora-%d&arch=$basearch --ignoregroups=true" % RELEASEVER)
    lines.append("repo --name=fedora-updates --mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-released-f%d&arch=$basearch --ignoregroups=true" % RELEASEVER)
    lines.append("repo --name=comps --baseurl=file://%s" % REPO_DIR)
    lines.append("")
    lines.append("%packages")
    for i in sorted(groups):
        lines.append("@%s" % i)
        lines.extend(read_comps_packages(COMPS_FILE, i))
        lines.append("")

    lines.extend(sorted(packages))
    lines.append("%end")
    return "\n".join(lines)


# remove old data first
kobo.shortcuts.run("rm -rfv %s" % WORK_DIR, stdout=True, can_fail=True)

os.makedirs(WORK_DIR)
os.makedirs(REPO_DIR)
os.makedirs(COMPOSE_DIR)

# write a new kickstart
kickstart = create_ks(groups=GROUPS, packages=PACKAGES)
kobo.shortcuts.save_to_file(KS_FILE, kickstart)

# create an empty repo to provide custom comps
kobo.shortcuts.run("createrepo . -g %s" % COMPS_FILE, workdir=REPO_DIR, stdout=True)

# run pungi
kobo.shortcuts.run("pungi --config=%s --name=FedoraServer --nosource --nodebuginfo --nosplitmedia --destdir=%s" % (KS_FILE, COMPOSE_DIR), stdout=True)
